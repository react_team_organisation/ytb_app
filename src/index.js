import _ from'lodash';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
const API_KEY = 'AIzaSyDfug9MJOsEFHtjeI7tZZCotff-tFPby90';

import SearchBar from './components/searchBar';
import VideoList from './components/videoList';
import VideoPanel from  './components/videoPanel';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            videos : [],
            selectedVideo: null,
        };
        this.videoSearch('surfboards');

    }

    videoSearch(term){
        YTSearch({key : API_KEY, term: term}, (videos) => {
                this.setState({ videos: videos, selectedVideo: videos[0]});
            }
        );
    }

    render() {
        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch}/>
                <VideoPanel video={this.state.selectedVideo}/>
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos}/>
            </div>);
    }
}
ReactDOM.render(<App />, document.querySelector('.container'));